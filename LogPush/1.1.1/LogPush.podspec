Pod::Spec.new do |s|

  s.name         = "LogPush"
  s.version      = "1.1.1"
  s.summary      = "LogPush iOS SDK."
  s.homepage     = "https://push.logbk.net/"
  s.license      = { :type => "Copyright", :text => "Copyright 2016 pLucky, Inc." }
  s.authors = { "pLucky, Inc." => "support@p-lucky.net" }
  s.platform     = :ios, "6.1"
  s.source = { :http => "https://logbk-public.s3.amazonaws.com/logpush_ios/LogPush-iOS-SDK_1.1.1.tgz" }
  s.framework  = "AdSupport"
  s.vendored_frameworks = 'LogPush.framework'

end
